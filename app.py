# -*- coding: utf-8 -*-

import uuid

from flask.app import Flask
from flask.globals import request
from flask.templating import render_template
import os
from werkzeug.utils import redirect

import config
import taobao
cli = taobao.APIClient(config.APP_KEY,config.APP_SEC)
cli.set_access_token(config.TOKEN)

def pic_uploader(filename):
    r = cli.upload.item_img_upload(num_iid=config.ITEM_ID, is_major='true', image=open(filename,"rb"))
    if 'item_img_upload_response' in r:
        return r['item_img_upload_response']['item_img']['url']
    else:
        return None

app = Flask(__name__)
@app.before_request
def before_request():
    pass

@app.teardown_request
def teardown_request(exception):
    pass

@app.route("/")
def index():
    return render_template('index.html')

@app.route('/upload',methods=['GET', 'POST'])
def upload():
    f = request.files['file']
    filename = "/tmp/"+uuid.uuid4().hex[:10]+"_"+f.filename
    f.save(filename)
    try:
        url = pic_uploader(filename)
    except taobao.APIError,e:
        os.remove(filename)
        return e.str2()
    os.remove(filename)
    if not url: return "Upload Error!"
    return redirect(url)

if __name__ == '__main__':
    app.run(debug=True)
